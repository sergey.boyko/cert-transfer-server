#!/bin/sh

# Print an error
echoerr() {
  echo "ERROR: $@" 1>&2;
}

# Print the script usage
print_usage() {
  echo ""
  echo "Usage: $0 [OPTION]..."
  echo ""
  echo "options:"
  echo "  -h --help     Show help "
  echo "  --libs=DIR    Specify a libs directory"
  echo "  --with-tests  Build and run tests"
  echo "  --prefix=DIR  Install cert-transfer-server and tests"
}

# Print formatted state
print_state() {
  title="| $1 |"
  edge=$(echo "$title" | sed 's/./-/g')
  echo ""
  echo "$edge"
  echo "$title"
  echo "$edge"
  echo ""
}

ENABLE_TESTS=OFF

# Process arguments
while [ "$1" != "" ]; do
  PARAM=`echo $1 | awk -F= '{print $1}'`
  VALUE=`echo $1 | sed 's/^[^=]*=//g'`
  case $PARAM in
    -h | --help)
      print_usage
      exit
      ;;

    --libs)
      LIBS=$VALUE ;;

    --with-tests)
      ENABLE_TESTS=ON ;;

    --prefix)
      PREFIX=$VALUE ;;

    -*)
      echoerr "unknown parameter \"$PARAM\""
      print_usage
      exit 1
      ;;
    esac
    shift
done

# Check "--libs" parameter
if [ "$LIBS" = "" ]
then
  echoerr "parameter \"--libs\" is required"
  print_usage
  exit 1
fi

if [ "$PREFIX" = "" ]
then
  echoerr "parameter \"--prefix\" is required"
fi

print_state "Configure"

cd ../
mkdir -p build
cd build

# Check if directory change finished successfully
if [ "$?" != "0" ]
then
  echoerr "failed to change directory"
  exit 1
fi

cmake -DCMAKE_PREFIX_PATH=$LIBS -DENABLE_TESTS=$ENABLE_TESTS -DCMAKE_INSTALL_PREFIX=$PREFIX ..

# Check if CMAKE configuration finished successfully
if [ "$?" != "0" ]
then
  echoerr "failed to configure CMAKE"
  exit 1
fi

print_state "Build"

make -j4

# Check if compilation finished successfully
if [ "$?" != "0" ]
then
  echoerr "failed to compile"
  exit 1
fi

print_state "Install"

make install

# Check if installation finished successfully
if [ "$?" != "0" ]
then
  echoerr "failed to install"
  exit 1
fi

print_state "Run tests"

#run tests
$PREFIX/tests/unit-tests

if [ "$?" != "0" ]
then
  echoerr "Tests filed"
  exit 1
fi

print_state "Success"
