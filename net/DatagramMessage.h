/**
* Created by Sergey O. Boyko on 30.01.19.
*/

#include <vector>
#include <DatagramMessage.h>

#pragma once

namespace net {

struct SharedDatagramMessage: DatagramMessageHeader {
  std::vector<char> m_data;
};

}
