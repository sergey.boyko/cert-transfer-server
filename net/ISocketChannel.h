/**
* Created by Sergey O. Boyko on 30.01.19.
*/

#pragma once

#include <cstdint>
#include <string_view>

#include "ISocketChannelListener.h"
#include "Endpoint.h"

namespace net {

class ISocketChannel {
public:
  explicit ISocketChannel(ISocketChannelListener *listener):
      m_listener(listener) {
  }

  virtual void Close() = 0;

  virtual bool Connected() = 0;

  virtual Endpoint GetRemoteEndpoint() = 0;

  virtual Endpoint GetLocalEndpoint() = 0;

  virtual void SendMessage(uint16_t message_type, std::string_view data) = 0;

  virtual void SendMessage(std::string_view data) = 0;

protected:
  ISocketChannelListener *m_listener;
};

}
