/**
* Created by Sergey O. Boyko on 30.01.19.
*/

#pragma once

#include <memory>

#include "DatagramMessage.h"

namespace net {

class ISocketChannel;

class ISocketChannelListener {
public:
  virtual void onConnected(const std::shared_ptr<ISocketChannel> &socket_channel) = 0;

  virtual void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
                         const DatagramMessage &message) = 0;

  virtual void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) = 0;

  virtual void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) = 0;
};

}
