//
// Created by bso on 26.08.18.
//

#pragma once

#include <string>

#include "base/Formatter.h"

namespace net {

struct Endpoint {
  /**
  * Default ctr
  */
  Endpoint() = default;

  /**
  * Local endpoint ctr
  * @param port
  */
  explicit Endpoint(uint16_t port):
      m_port(port) {
  }

  /**
  * Remote endpoint ctr
  * @param ip
  * @param port
  */
  explicit Endpoint(std::string_view ip, uint16_t port):
      m_ip(ip),
      m_port(port) {
  }

  /**
  * Get endpoint as string. Format:
  * "IP:port"
  * @return endpoint as string
  */
  inline std::string AsString() noexcept {
    return core::base::Format("%:%", m_ip, m_port);
  }

  /**
  * IP address
  */
  std::string m_ip = "127.0.0.1";

  /**
  * Port
  */
  uint16_t m_port = 0;
};

} //end of core::net


