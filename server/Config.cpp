/**
* Created by Sergey O. Boyko on 26.01.19.
*/

#include "Config.h"

#include <fstream>

Json::Value Config;

void LoadConfig(const std::string &config) {
  Json::Reader reader;
  std::ifstream in(config);

  if(!reader.parse(in, Config)) {
    throw std::runtime_error("Could't parse config: " + config);
  }
}
