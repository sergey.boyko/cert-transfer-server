/**
* Created by Sergey O. Boyko on 26.01.19.
*/

#pragma once

#include <third-party/jsoncpp/json.h>

extern Json::Value Config;

void LoadConfig(const std::string &config);
