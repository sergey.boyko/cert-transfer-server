/**
* Created by Sergey O. Boyko on 20.01.19.
*/

#include <sys/stat.h>

#include <boost/program_options.hpp>
#include <base/Logger.h>
#include <base/LoggerLevel.h>

#include "Config.h"

//TODO remove it
extern "C" {
#include "SecurityReliableDatagramProtocol.h"
}

#include "NoiseDefinitions.h"
//TODO remove

namespace po = boost::program_options;

int main(int argc, char *argv[]) {
  bool daemon = false;
  std::string config;

  po::options_description options("General options");
  options.add_options()
      ("help,h", "Show help")
      ("daemon,d", "Start the application as a daemon")
      ("config,c",
       po::value<std::string>(&config)->required(),
       "Set the full path to the configuration file");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, options), vm);

  if (vm.count("help")) {
    std::cout << options << std::endl;
    return 0;
  }

  po::notify(vm);

  if (vm.count("daemon")) {
    daemon = true;
  }

  LoadConfig(config);

  if (daemon) {
    auto pid = fork();

    if (pid < 0) {
      std::cerr << "Couldn't start a daemon" << std::endl;
      return 1;
    } else if (pid == 0) { // child
      umask(0);
      setsid();

      close(STDIN_FILENO);
      close(STDOUT_FILENO);
      close(STDERR_FILENO);
    } else { // parent
      return 0;
    }
  }

  if (!Config["clear-log"].empty()
      && Config["clear-log"].asBool()) {
    Logger::ClearLogs();
  }

  if (!Config["console-output"].empty()
      && Config["console-output"].asBool()) {
    Logger::UseConsoleOutput();
  }

  unsigned int log_level = core::base::LoggerLevel::None;
  if (!Config["log-level"].empty()) {
    log_level = Config["log-level"].asUInt();
  }
  Logger::SetLevel(log_level);

  if (!Config["log-file"].empty()) {
    Logger::UseLogFile(Config["log-file"].asString());
  }

  Logger::SetModuleName("cert-transfer-server");

  if (!Logger::InitLogger()) {
    std::cerr << Logger::GetLastError() << std::endl;
    return 1;
  }
}
