//
// Created by bso on 19.02.19.
//

#include <gtest/gtest.h>
#include <boost/program_options.hpp>

#include "CryptoTest.h"
#include "base/Logger.h"
#include "base/LoggerLevel.h"

namespace po = boost::program_options;

int main(int argc, char *argv[]) {
  // Logger::UseConsoleOutput();
  Logger::SetLevel(core::base::LoggerLevel::Debug);
  Logger::SetModuleName("tests");
  if (!Logger::InitLogger()) {
    std::cerr << Logger::GetLastError() << std::endl;
    return 1;
  }

  if (noise_init() != NOISE_ERROR_NONE) {
    return 1;
  }


  NoiseHandshakeState *m_noise_handshake_state;
  auto error =
      noise_handshakestate_new_by_name(
          &m_noise_handshake_state,
          "Noise_X_25519_AESGCM_SHA256",
          NOISE_ROLE_RESPONDER);

  if (!noise_handshakestate_needs_local_keypair(m_noise_handshake_state)) {
    std::cout << "wtf";
    return 1;
  }


  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
