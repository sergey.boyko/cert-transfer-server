//
// Created by bso on 19.02.19.
//

#pragma once

#include <random>
#include <gtest/gtest.h>
#include "ServerNoiseCryptoContext.h"
#include "ClientNoiseCryptoContext.h"
#include "CryptoTestDefinitions.h"

using namespace crypto;

class TestFNoiseCryptoContext: public ::testing::Test {
public:
  // m_client_ctx have a default ctr
  TestFNoiseCryptoContext():
      m_server_ctx(
          {"Noise"}, // Prefix names
          {"NN", "XX", "XK", "KK", "NK"}, // Patterns
          {"25519", "448"}, // DH names
          {"ChaChaPoly", "AESGCM"}, // Cipher names
          {"SHA256", "BLAKE2s"} // Hash names
      ) {
  }

  ~TestFNoiseCryptoContext() = default;

  void SetUp() { /* called before every test */ }

  void TearDown() { /* called after every test */ }

protected:
  ServerNoiseCryptoContext m_server_ctx;
  ClientNoiseCryptoContext m_client_ctx;
  /* none yet */
};

TEST(CryptoContextTest, InitializeEmptyServerCryptoContext) {
  ASSERT_ANY_THROW(ServerNoiseCryptoContext ctx({""}, {""}, {""}, {""}, {""}));
}

TEST(CryptoContextTest, InitializePskServerCryptoContext) {
  ASSERT_ANY_THROW(ServerNoiseCryptoContext
                       ctx({"NoisePSK"},
                           {"NN", "XX"},
                           {"25519", "448"},
                           {"ChaChaPoly", "AESGCM"},
                           {"SHA256", "BLAKE2s"})
  );
}

TEST_F(TestFNoiseCryptoContext, IncorrectSettings) {
  // generate a random settings data
  std::vector<char> settings(sizeof(NoiseProtocolId));
  std::random_device rd;
  std::generate(settings.begin(), settings.end(), [&rd]() { return rd(); });
  ASSERT_FALSE(m_server_ctx.Init(settings));
}

TEST_F(TestFNoiseCryptoContext, IncorrectLocalPrivateKey) {
  ASSERT_TRUE(m_server_ctx.Init(m_server_ctx.MakeSettings(DhCurves::DH_CURVE448)));
  ASSERT_TRUE(m_server_ctx.GetSelectedCurve() == DhCurves::DH_CURVE448);
  ASSERT_TRUE(m_server_ctx.GetHandshakeState() == HandshakeStates::KEYS_EXCHANGE);

  ASSERT_FALSE(m_server_ctx.SetLocalPrivateKey(KeyToVector(ServerPrivateKey25519)));
}

TEST_F(TestFNoiseCryptoContext, IncorrectRemoteKey) {
  ASSERT_TRUE(m_server_ctx.Init(m_server_ctx.MakeSettings(DhCurves::DH_CURVE448)));
  ASSERT_TRUE(m_server_ctx.GetSelectedCurve() == DhCurves::DH_CURVE448);
  ASSERT_TRUE(m_server_ctx.GetHandshakeState() == HandshakeStates::KEYS_EXCHANGE);

  ASSERT_TRUE(m_server_ctx.SetLocalPrivateKey(KeyToVector(ServerPrivateKey448)));
  ASSERT_FALSE(m_server_ctx.SetRemotePublicKey(KeyToVector(ClientPublicKey25519)));
}

TEST_F(TestFNoiseCryptoContext, SuccessInit25519) {
  ASSERT_TRUE(m_server_ctx.Init(m_server_ctx.MakeSettings(DhCurves::DH_CURVE25519)));
  ASSERT_TRUE(m_server_ctx.GetSelectedCurve() == DhCurves::DH_CURVE25519);
  ASSERT_TRUE(m_server_ctx.GetHandshakeState() == HandshakeStates::KEYS_EXCHANGE);

  ASSERT_TRUE(m_server_ctx.SetLocalPrivateKey(KeyToVector(ServerPrivateKey25519)));
  ASSERT_TRUE(m_server_ctx.SetRemotePublicKey(KeyToVector(ClientPublicKey25519)));

  ASSERT_TRUE(m_server_ctx.StartHandshake());
}

TEST_F(TestFNoiseCryptoContext, SuccessInit448) {
  ASSERT_TRUE(m_server_ctx.Init(m_server_ctx.MakeSettings(DhCurves::DH_CURVE448)));
  ASSERT_TRUE(m_server_ctx.GetSelectedCurve() == DhCurves::DH_CURVE448);
  ASSERT_TRUE(m_server_ctx.GetHandshakeState() == HandshakeStates::KEYS_EXCHANGE);
  // set private key
  ASSERT_TRUE(m_server_ctx.SetLocalPrivateKey(KeyToVector(ServerPrivateKey448)));
  ASSERT_TRUE(m_server_ctx.SetRemotePublicKey(KeyToVector(ClientPublicKey448)));

  ASSERT_TRUE(m_server_ctx.StartHandshake());
}

TEST_F(TestFNoiseCryptoContext, SuccessHandshakeNN448) {
  NoiseProtocolId nid {
      NOISE_PREFIX_STANDARD, /**< Protocol name prefix */
      NOISE_PATTERN_NN, /**< Handshake pattern */
      NOISE_DH_CURVE448, /**< Diffie-Hellman algorithm identifier */
      NOISE_CIPHER_AESGCM, /**< Cipher algorithm identifier */
      NOISE_HASH_SHA256, /**< Hash algorithm identifier */
  };

  ToNetworkByteOrder(nid);
  auto settings = ToVector(nid);

  ASSERT_TRUE(m_client_ctx.Init(settings));
  ASSERT_TRUE(m_server_ctx.Init(settings));

  ASSERT_TRUE(m_server_ctx.SetLocalPrivateKey(KeyToVector(ServerPrivateKey448)));
  ASSERT_TRUE(m_server_ctx.SetRemotePublicKey(KeyToVector(ClientPublicKey448)));

  ASSERT_TRUE(m_client_ctx.SetLocalPrivateKey(KeyToVector(ClientPrivateKey448)));
  ASSERT_TRUE(m_client_ctx.SetRemotePublicKey(KeyToVector(ServerPublicKey448)));

  ASSERT_TRUE(m_server_ctx.StartHandshake());
  ASSERT_TRUE(m_client_ctx.StartHandshake());

  auto do_handshake =
      [](NoiseCryptoContext &read_side, NoiseCryptoContext &write_side) {
        // write_side must be checked already (before the lambda has called)
        ASSERT_TRUE(read_side.GetHandshakeState()
                    == HandshakeStates::READ_HANDSHAKE_DATA);

        // TODO replace std::vector<char> with special alias
        std::vector<char> data;
        ASSERT_TRUE(write_side.GetHandshakeData(data));
        ASSERT_TRUE(read_side.OnHandshakeData(data));
      };

  auto handshake_finished = false;
  while(!handshake_finished) {
    if (m_server_ctx.GetHandshakeState() == HandshakeStates::SEND_HANDSHAKE_DATA) {
      // m_client_ctx is read_side, m_server_ctx is write_side
      do_handshake(m_client_ctx, m_server_ctx);
    } else if (m_client_ctx.GetHandshakeState()
               == HandshakeStates::SEND_HANDSHAKE_DATA) {
      // m_server_ctx is read_side, m_client_ctx is write_side
      do_handshake(m_server_ctx, m_client_ctx);
    } else {
      ASSERT_TRUE(m_server_ctx.GetHandshakeState() == HandshakeStates::SUCCESS
                  && m_client_ctx.GetHandshakeState() == HandshakeStates::SUCCESS);
      handshake_finished = true;
    }
  }
}
