/**
* Created by Sergey O. Boyko on 30.01.19.
*/

#include <cstring>

#include <base/Logger.h>
#include <base/Checking.h>

#include "ChangeByteOrder.h"
#include "NoiseCryptoContext.h"
#include "CryptoDefinitions.h"

namespace crypto {

NoiseCryptoContext::~NoiseCryptoContext() {
  if (m_noise_handshake_state) {
    noise_handshakestate_free(m_noise_handshake_state);
    m_noise_handshake_state = nullptr;
  }
}

std::vector<char> NoiseCryptoContext::MakeSettings(DhCurves::Curve curve) {
  auto noise_curve =
      curve == DhCurves::DH_CURVE25519
      ? NOISE_DH_CURVE25519
      : NOISE_DH_CURVE448;

  NoiseProtocolId nid {
      NOISE_PREFIX_STANDARD, /**< Protocol name prefix */
      NOISE_PATTERN_KK, /**< Handshake pattern */
      noise_curve, /**< Diffie-Hellman algorithm identifier */
      NOISE_CIPHER_AESGCM, /**< Cipher algorithm identifier */
      NOISE_HASH_SHA256, /**< Hash algorithm identifier */
      0 /**< Hybrid forward secrecy algorithm identifier */
  };

  ToNetworkByteOrder(nid);

  return ToVector(nid);
}


bool NoiseCryptoContext::Init(const std::vector<char> &settings) {
  if (m_handshake_state != HandshakeStates::IDLE) {
    LOG_E("Context already initialized. Current state = %", m_handshake_state);
    return false;
  }

  NoiseProtocolId nid;
  if (settings.size() != sizeof(NoiseProtocolId)) {
    LOG_E("Incorrect settings length: %", settings.size());
    return false;
  }

  memcpy(&nid, settings.data(), settings.size());
  ToHostByteOrder(nid);

  if (!_VerifyNoiseIds(nid)) {
    return false;
  }

  auto error =
      noise_handshakestate_new_by_id(&m_noise_handshake_state, &nid, _GetNoiseRole());
  if (error != NOISE_ERROR_NONE) {
    LOG_E("Couldn't create NoiseHandshakeState by NoiseProtocolId. Error: '%'",
          GetNoiseError(error));
    return false;
  }

  m_selected_dh_curve =
      nid.dh_id == NOISE_DH_CURVE448
      ? DhCurves::DH_CURVE448
      : DhCurves::DH_CURVE25519;

  m_handshake_state = HandshakeStates::KEYS_EXCHANGE;
  return true;
}


bool NoiseCryptoContext::StartHandshake() {
  if (m_handshake_state != HandshakeStates::KEYS_EXCHANGE) {
    LOG_E("Incorrect current state: '%'. KEYS_EXCHANGE expected", m_handshake_state);
    return false;
  }

  if ((IsRemotePublicKeyNeeded() && !IsRemotePublicKey())
      || (IsLocalPrivateKeyNeeded() && !IsLocalPrivateKey())) {
    LOG_E("Remote public key (set=%) or local private key (set=%) is not set",
          IsRemotePublicKey(),
          IsLocalPrivateKey());
    return false;
  }

  auto error = noise_handshakestate_start(m_noise_handshake_state);
  if (error != NOISE_ERROR_NONE) {
    LOG_E("Couldn't start a handshake. Error: '%'", GetNoiseError(error));
    return false;
  }

  _CheckHandshakeState();
  return true;
}


bool NoiseCryptoContext::SetLocalPrivateKey(std::vector<char> private_key) {
  if (m_handshake_state != HandshakeStates::KEYS_EXCHANGE) {
    LOG_E("Incorrect current state: '%'. KEYS_EXCHANGE expected", m_handshake_state);
    return false;
  }

  if (!IsLocalPrivateKeyNeeded()) {
    LOG_D("there is no need to local private key");
    return true;
  }

  auto *dh = noise_handshakestate_get_local_keypair_dh(m_noise_handshake_state);

  auto key_length = noise_dhstate_get_private_key_length(dh);
  if (private_key.size() != key_length) {
    LOG_E("Incorrect local private key length (%u), expected %u",
          private_key.size(),
          key_length);
    return false;
  }

  auto error = noise_dhstate_set_keypair_private(
      dh,
      reinterpret_cast<uint8_t *>(private_key.data()),
      private_key.size());

  if (error == NOISE_ERROR_NONE) {
    m_is_local_private_key = true;
  } else {
    LOG_E("Couldn't set local private key. Error: %", GetNoiseError(error));
  }

  return error == NOISE_ERROR_NONE;
}


bool NoiseCryptoContext::SetRemotePublicKey(std::vector<char> key) {
  if (m_handshake_state != HandshakeStates::KEYS_EXCHANGE) {
    LOG_E("Incorrect current state: '%'. KEYS_EXCHANGE expected", m_handshake_state);
    return false;
  }

  if (!IsRemotePublicKeyNeeded()) {
    LOG_D("there is no need to remote public key");
    return true;
  }

  auto *dh = noise_handshakestate_get_remote_public_key_dh(m_noise_handshake_state);

  auto key_length = noise_dhstate_get_public_key_length(dh);
  if (key.size() != key_length) {
    LOG_E("Incorrect remote public key length (%u), expected %u",
          key.size(),
          key_length);
    return false;
  }

  auto error = noise_dhstate_set_public_key(
      dh,
      reinterpret_cast<uint8_t *>(key.data()),
      key.size());

  if (error == NOISE_ERROR_NONE) {
    m_is_remote_public_key = true;
  } else {
    LOG_E("Couldn't set remote public key. Error: %", GetNoiseError(error));
  }

  return error == NOISE_ERROR_NONE;
}


bool NoiseCryptoContext::IsLocalPrivateKeyNeeded() {
  CHECK(m_noise_handshake_state, "noise handshake state is nullptr");

  return static_cast<bool>(
      noise_handshakestate_needs_local_keypair(m_noise_handshake_state));
}


bool NoiseCryptoContext::IsRemotePublicKeyNeeded() {
  CHECK(m_noise_handshake_state, "noise handshake state is nullptr");

  return static_cast<bool>(
      noise_handshakestate_needs_remote_public_key(m_noise_handshake_state));
}


bool NoiseCryptoContext::OnHandshakeData(std::vector<char> &data) {
  if (m_handshake_state != HandshakeStates::READ_HANDSHAKE_DATA) {
    LOG_E("Incorrect current state: '%'. Should be READ_HANDSHAKE_DATA",
          m_handshake_state);
    return false;
  }

  NoiseBuffer buffer;
  noise_buffer_set_input(buffer,
                         reinterpret_cast<uint8_t *>(data.data()),
                         data.size());

  auto error
      = noise_handshakestate_read_message(m_noise_handshake_state, &buffer, nullptr);
  if (error != NOISE_ERROR_NONE) {
    LOG_E("Couldn't read handshake data. Error = %", GetNoiseError(error));
    return false;
  }

  _CheckHandshakeState();
  return true;
}


bool NoiseCryptoContext::GetHandshakeData(std::vector<char> &data) {
  if (m_handshake_state != HandshakeStates::SEND_HANDSHAKE_DATA) {
    LOG_E("Incorrect current state: '%'. SEND_HANDSHAKE_DATA expected",
          m_handshake_state);
    return false;
  }

  data.clear();
  data.resize(NOISE_MAX_PAYLOAD_LEN, 0);

  NoiseBuffer buffer;
  noise_buffer_set_output(buffer,
                          reinterpret_cast<uint8_t *>(data.data()),
                          data.size());
  auto error
      = noise_handshakestate_write_message(m_noise_handshake_state, &buffer, nullptr);
  if (error != NOISE_ERROR_NONE) {
    LOG_E("Couldn't write handshake data to container. Error: %",
          GetNoiseError(error));
    return false;
  }

  _CheckHandshakeState();
  return true;
}


bool NoiseCryptoContext::EncryptData(std::vector<char> &data) {
  if (m_handshake_state != HandshakeStates::SUCCESS) {
    LOG_E("Handshake is not finished yet");
    return false;
  }

  auto original_size = data.size();

  // TODO fix
  // expand to NOISE_MAX_PAYLOAD_LEN number of bytes
  // (first original bytes will not be cleared)
  data.resize(NOISE_MAX_PAYLOAD_LEN, 0);

  NoiseBuffer buffer;
  noise_buffer_set_inout(
      buffer,
      reinterpret_cast<uint8_t *>(data.data()),
      original_size,
      NOISE_MAX_PAYLOAD_LEN);

  auto error = noise_cipherstate_encrypt(m_encrypt_cipher, &buffer);
  if (error != NOISE_ERROR_NONE) {
    LOG_E("Couldn't encrypt data. Error: %", GetNoiseError(error));
    return false;
  }

  data.resize(buffer.size);
  return true;
}


bool NoiseCryptoContext::DecryptData(std::vector<char> &data) {
  if (m_handshake_state != HandshakeStates::SUCCESS) {
    LOG_E("Handshake is not finished yet");
    return false;
  }

  auto original_size = data.size();

  // TODO fix
  // expand to NOISE_MAX_PAYLOAD_LEN number of bytes
  // (first original bytes will not be cleared)
  data.resize(NOISE_MAX_PAYLOAD_LEN, 0);

  NoiseBuffer buffer;
  noise_buffer_set_inout(
      buffer,
      reinterpret_cast<uint8_t *>(data.data()),
      original_size,
      NOISE_MAX_PAYLOAD_LEN);

  auto error = noise_cipherstate_decrypt(m_decrypt_cipher, &buffer);
  if (error != NOISE_ERROR_NONE) {
    LOG_E("Couldn't decrypt data. Error: %", GetNoiseError(error));
    return false;
  }

  data.resize(buffer.size);
  return true;
}


void NoiseCryptoContext::_CheckHandshakeState() {
  auto action = noise_handshakestate_get_action(m_noise_handshake_state);
  switch(action) {
    case NOISE_ACTION_WRITE_MESSAGE: {
      m_handshake_state = HandshakeStates::SEND_HANDSHAKE_DATA;
      return;
    }

    case NOISE_ACTION_READ_MESSAGE: {
      m_handshake_state = HandshakeStates::READ_HANDSHAKE_DATA;
      return;
    }

    case NOISE_ACTION_SPLIT: {
      // need split and check result
      auto error = noise_handshakestate_split(
          m_noise_handshake_state, &m_encrypt_cipher, &m_decrypt_cipher);
      if (error == NOISE_ERROR_NONE) {
        m_handshake_state = HandshakeStates::SUCCESS;
      } else {
        LOG_E("Couldn't split ciphers. Error = '%'", GetNoiseError(error));
        m_handshake_state = HandshakeStates::ERROR;
      }

      _ResetHandshake();
      return;
    }

    case NOISE_ACTION_COMPLETE: {
      _ResetHandshake();
      m_handshake_state = HandshakeStates::SUCCESS;
      return;
    }

    default: {
      _ResetHandshake();
      m_handshake_state = HandshakeStates::ERROR;
      return;
    }
  }
}


void NoiseCryptoContext::_ResetHandshake() {
  noise_handshakestate_free(m_noise_handshake_state);
  m_noise_handshake_state = nullptr;
}

}
