/**
* Created by Sergey O. Boyko on 14.02.19.
*/

#include <base/Logger.h>
#include <base/Checking.h>

#include "ICryptoContext.h"
#include "Base64.h"

bool crypto::ICryptoContext::SetLocalPrivateKey(
    const std::experimental::filesystem::path &key_path) {
  namespace fs = std::experimental::filesystem;
  if (!fs::is_regular_file(key_path)) {
    LOG_E("Private key path points to not a regular file. '%'",
          key_path.string());
    return false;
  }

  auto private_key_size = fs::file_size(key_path);
  if (private_key_size > MAX_DH_KEY_LEN) {
    LOG_E("Incorrect file size = %",
          private_key_size);
    return false;
  }

  std::ifstream fstream(key_path, std::ios::binary);
  std::vector<char> key_data(std::istreambuf_iterator<char>(fstream), {});

  return SetLocalPrivateKey(key_data);
}
