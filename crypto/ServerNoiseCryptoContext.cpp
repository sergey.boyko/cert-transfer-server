/**
* Created by Sergey O. Boyko on 23.02.19.
*/

#include <cstring>
#include "base/Checking.h"
#include "ServerNoiseCryptoContext.h"

crypto::ServerNoiseCryptoContext::ServerNoiseCryptoContext(
    const std::vector<std::string> &available_protocol_prefix_names,
    const std::vector<std::string> &available_pattern_names,
    const std::vector<std::string> &available_dh_names,
    const std::vector<std::string> &available_cipher_names,
    const std::vector<std::string> &available_hash_alg_names) {
  auto fill_available_ids =
      [](const std::vector<std::string> &names,
         std::unordered_set<int> &dest_id_container,
         int category) {
        // convert names to ids
        for(const auto &name : names) {
          auto id = noise_name_to_id(category, name.data(), name.size());
          CHECK_D(id, "incorrect name ('%'), category (%)", name, category);

          CHECK_D(category != NOISE_PREFIX_CATEGORY
                  || id != NOISE_PREFIX_PSK,
                  "NOISE_PREFIX_PSK is not supported");

          auto success = dest_id_container.insert(id).second;
          if (!success) {
            LOG_E("there is already the same identifier (%:'%')", id, name);
          }
        }
      };

  fill_available_ids(available_protocol_prefix_names,
                     m_available_protocol_prefix_ids,
                     NOISE_PREFIX_CATEGORY);

  fill_available_ids(available_pattern_names,
                     m_available_pattern_ids,
                     NOISE_PATTERN_CATEGORY);

  fill_available_ids(available_dh_names,
                     m_available_dh_ids,
                     NOISE_DH_CATEGORY);

  fill_available_ids(available_cipher_names,
                     m_available_cipher_ids,
                     NOISE_CIPHER_CATEGORY);

  fill_available_ids(available_hash_alg_names,
                     m_available_hash_alg_ids,
                     NOISE_HASH_CATEGORY);
}


bool crypto::ServerNoiseCryptoContext::_VerifyNoiseIds(
    const NoiseProtocolId &nid) {
  // even if the ids is wrong noise_handshakestate_new_by_id will detect it
  if (nid.prefix_id == NOISE_PREFIX_PSK) {
    LOG_E("NOISE_PREFIX_PSK is not supported");
  }

  if (m_available_protocol_prefix_ids.find(nid.prefix_id)
      == m_available_protocol_prefix_ids.end()) {
    LOG_E("Received unavailable prefix id: %", nid.prefix_id);
    return false;
  }

  if (m_available_pattern_ids.find(nid.pattern_id)
      == m_available_pattern_ids.end()) {
    LOG_E("Received unavailable pattern id: %", nid.pattern_id);
    return false;
  }

  if (m_available_dh_ids.find(nid.dh_id)
      == m_available_dh_ids.end()) {
    LOG_E("Received unavailable DH id: %", nid.dh_id);
    return false;
  }

  if (m_available_cipher_ids.find(nid.cipher_id)
      == m_available_cipher_ids.end()) {
    LOG_E("Received unavailable cipher id: %", nid.cipher_id);
    return false;
  }

  if (m_available_hash_alg_ids.find(nid.hash_id)
      == m_available_hash_alg_ids.end()) {
    LOG_E("Received unavailable hash id: %", nid.hash_id);
    return false;
  }

  return true;
}


int crypto::ServerNoiseCryptoContext::_GetNoiseRole() {
  return NOISE_ROLE_RESPONDER;
}
