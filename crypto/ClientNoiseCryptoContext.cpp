/**
* Created by Sergey O. Boyko on 23.02.19.
*/

#include <cstring>
#include "ClientNoiseCryptoContext.h"

bool crypto::ClientNoiseCryptoContext::_VerifyNoiseIds(const NoiseProtocolId &nid) {
  return true;
}


int crypto::ClientNoiseCryptoContext::_GetNoiseRole() {
  return NOISE_ROLE_INITIATOR;
}
