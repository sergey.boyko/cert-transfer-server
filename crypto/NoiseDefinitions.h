/**
* Created by Sergey O. Boyko on 06.02.19.
*/

#pragma once

#include <noise/protocol.h>

#include "base/Logger.h"
#include "ChangeByteOrder.h"

namespace crypto {

inline void ToNetworkByteOrder(NoiseProtocolId &nid) {
  _Htonl(reinterpret_cast<uint32_t *>(&nid.prefix_id));
  _Htonl(reinterpret_cast<uint32_t *>(&nid.pattern_id));
  _Htonl(reinterpret_cast<uint32_t *>(&nid.dh_id));
  _Htonl(reinterpret_cast<uint32_t *>(&nid.cipher_id));
  _Htonl(reinterpret_cast<uint32_t *>(&nid.hash_id));
  _Htonl(reinterpret_cast<uint32_t *>(&nid.hybrid_id));

  for(int i : nid.reserved) {
    _Htonl(reinterpret_cast<uint32_t *>(&i));
  }
}

inline void ToHostByteOrder(NoiseProtocolId &nid) {
  _Ntohl(reinterpret_cast<uint32_t *>(&nid.prefix_id));
  _Ntohl(reinterpret_cast<uint32_t *>(&nid.pattern_id));
  _Ntohl(reinterpret_cast<uint32_t *>(&nid.dh_id));
  _Ntohl(reinterpret_cast<uint32_t *>(&nid.cipher_id));
  _Ntohl(reinterpret_cast<uint32_t *>(&nid.hash_id));
  _Ntohl(reinterpret_cast<uint32_t *>(&nid.hybrid_id));

  for(int i : nid.reserved) {
    _Ntohl(reinterpret_cast<uint32_t *>(&i));
  }
}

inline std::vector<char> ToVector(const NoiseProtocolId &nid) {
  const char *nid_ptr = reinterpret_cast<const char *>(&nid);
  return {nid_ptr, nid_ptr + sizeof(NoiseProtocolId)};
}

enum NoiseDefinitions {
  NOISE_ERROR_BUF_SIZE = 80,
};

inline std::string GetNoiseError(int error_code) {
  char error_buffer[NOISE_ERROR_BUF_SIZE];
  noise_strerror(error_code, error_buffer, NOISE_ERROR_BUF_SIZE);
  return error_buffer;
}

inline bool InitNoise() {
  auto error = noise_init();
  if (error != NOISE_ERROR_NONE) {
    LOG_E("Couldn't initialize Noize-C lib. Error: '%'", GetNoiseError(error));
    return false;
  }

  return true;
}

}
