/**
* Created by Sergey O. Boyko on 23.02.19.
*/

#pragma once

#include "NoiseCryptoContext.h"

namespace crypto {

class ClientNoiseCryptoContext: public NoiseCryptoContext {
public:
  /**
  * Default ctor
  */
  ClientNoiseCryptoContext() = default;

  /**
  * Virtual dtor
  */
  virtual ~ClientNoiseCryptoContext() = default;

protected:
  /**
  * @overload
  */
  bool _VerifyNoiseIds(const NoiseProtocolId &nid) final;

  /**
  * @overload
  */
  int _GetNoiseRole() final;
};

}

