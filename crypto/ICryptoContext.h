/**
* Created by Sergey O. Boyko on 30.01.19.
*/

#pragma once

#include <string>
#include <string_view>
#include <vector>
#include <experimental/filesystem>
#include <unordered_map>

#include <base/Checking.h>
#include "CryptoDefinitions.h"

namespace crypto {

class ICryptoContext {
public:
  ICryptoContext() = default;

  virtual ~ICryptoContext() = default;

  virtual std::vector<char> MakeSettings(DhCurves::Curve curve) = 0;

  virtual bool Init(const std::vector<char> &settings) = 0;

  virtual bool StartHandshake() = 0;

  /**
  * Private key must consists of binary data.
  * ! Must be called before StartHandshake method !
  * @param public_key - binary data
  * @param key_data - binary data
  * @return - always true
  */
  virtual bool SetLocalPrivateKey(std::vector<char> key_data) = 0;

  /**
  * Load public and private keys from files.
  * Private key must consists of binary data.
  * Public key must consists of Base64 encoded data.
  * ! Must be called before InitKeys() !
  * @param public_key - key path
  * @param key_path - key path
  * @return
  */
  virtual bool SetLocalPrivateKey(const std::experimental::filesystem::path &key_path);

  /**
  * Remote public key must satisfy settings (one of the DhCurves)
  * ! Must be called before InitKeys() !
  * @param key_data - remote public key data (Base64 decoded data)
  * @return
  */
  virtual bool SetRemotePublicKey(std::vector<char> key_data) = 0;

  virtual bool IsLocalPrivateKeyNeeded() = 0;

  virtual bool IsRemotePublicKeyNeeded() = 0;

  /**
  * Process received remote handshake data.
  * Data is not const because it can required by using library (Noise-C for example)
  * @param data - received handshake data
  * @return - true if received data is correct
  *           and the m_handshake_state state is READ_HANDSHAKE_DATA
  *           else false
  */
  virtual bool OnHandshakeData(std::vector<char> &data) = 0;

  /**
  * Get handshake data for sending to remote side
  * @param data - buffer to sending handshake data
  * @return true if the m_handshake_state state is SEND_HANDSHAKE_DATA
  */
  virtual bool GetHandshakeData(std::vector<char> &data) = 0;

  virtual bool EncryptData(std::vector<char> &data) = 0;

  virtual bool DecryptData(std::vector<char> &data) = 0;

  inline HandshakeStates::State GetHandshakeState() {
    return m_handshake_state;
  }

  inline DhCurves::Curve GetSelectedCurve() {
    CHECK_D(m_handshake_state != HandshakeStates::IDLE, "Incorrect state to call GetSelectedCurve()");
    return m_selected_dh_curve;
  }

  inline bool IsLocalPrivateKey() {
    return m_is_local_private_key;
  }

  inline bool IsRemotePublicKey() {
    return m_is_remote_public_key;
  }

protected:
  HandshakeStates::State m_handshake_state = HandshakeStates::IDLE;
  DhCurves::Curve m_selected_dh_curve = DhCurves::DH_CURVE448;

  bool m_is_local_private_key = false;
  bool m_is_remote_public_key = false;
};

}
