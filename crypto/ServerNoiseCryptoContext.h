/**
* Created by Sergey O. Boyko on 23.02.19.
*/

#pragma once

#include "NoiseCryptoContext.h"

namespace crypto {

class ServerNoiseCryptoContext: public NoiseCryptoContext {
public:
  /**
  * Ctor
  * @param available_protocol_prefix_names
  * @param available_pattern_names
  * @param available_dh_names
  * @param available_cipher_names
  * @param available_hash_alg_names
  */
  explicit ServerNoiseCryptoContext(
      const std::vector<std::string> &available_protocol_prefix_names,
      const std::vector<std::string> &available_pattern_names,
      const std::vector<std::string> &available_dh_names,
      const std::vector<std::string> &available_cipher_names,
      const std::vector<std::string> &available_hash_alg_names);

  /**
  * Virtual dtor
  */
  virtual ~ServerNoiseCryptoContext() = default;

protected:
  /**
  * @overload
  */
  bool _VerifyNoiseIds(const NoiseProtocolId &nid) final;

  /**
  * @overload
  */
  int _GetNoiseRole() final;

  std::unordered_set<int> m_available_protocol_prefix_ids;
  std::unordered_set<int> m_available_pattern_ids;
  std::unordered_set<int> m_available_dh_ids;
  std::unordered_set<int> m_available_cipher_ids;
  std::unordered_set<int> m_available_hash_alg_ids;
};

}
