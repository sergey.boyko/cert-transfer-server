/**
* Created by Sergey O. Boyko on 10.02.19.
*/

#pragma once

namespace crypto {

enum CryptoDefinitions {
  MAX_DH_KEY_LEN = 2048,
};

namespace HandshakeStates {
enum State {
  IDLE = 0,
  KEYS_EXCHANGE,
  SEND_HANDSHAKE_DATA,
  READ_HANDSHAKE_DATA,
  ERROR,
  SUCCESS
};
}

namespace DhCurves {
enum Curve {
  DH_CURVE448 = 0,
  DH_CURVE25519
};
}

}
