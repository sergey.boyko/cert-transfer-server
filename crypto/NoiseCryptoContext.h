/**
* Created by Sergey O. Boyko on 30.01.19.
*/

#pragma once

#include <unordered_set>

#include "ICryptoContext.h"
#include "NoiseDefinitions.h"

namespace crypto {

class NoiseCryptoContext: public ICryptoContext {
public:
  /**
  * Default ctor
  */
  NoiseCryptoContext() = default;

  /**
  * Virtual dtor
  */
  virtual ~NoiseCryptoContext();

  /**
  * TODO add other params
  * @overload
  */
  std::vector<char> MakeSettings(DhCurves::Curve curve) final;

  /**
  * @overload
  */
  bool Init(const std::vector<char> &settings) final;

  /**
  * @overload
  */
  bool StartHandshake() final;

  /**
  * @overload
  */
  bool SetLocalPrivateKey(std::vector<char> private_key) final;

  /**
  * @overload
  */
  bool SetRemotePublicKey(std::vector<char> key) final;

  /**
  * @overload
  */
  bool IsLocalPrivateKeyNeeded();

  /**
  * @overload
  */
  bool IsRemotePublicKeyNeeded();

  /**
  * @overload
  * data is not const because noise_buffer_set_input can't take const ptr
  */
  bool OnHandshakeData(std::vector<char> &data) final;

  /**
  * @overload
  */
  bool GetHandshakeData(std::vector<char> &data) final;

  /**
  * @overload
  */
  bool EncryptData(std::vector<char> &data) final;

  /**
  * @overload
  */
  bool DecryptData(std::vector<char> &data) final;

protected:
  /**
  * Verify NoiseProtocolId settings for connection
  * @param nid - settings as NoiseProtocolId
  * @return - true if settings is verified correctly
  */
  virtual bool _VerifyNoiseIds(const NoiseProtocolId &nid) = 0;

  /**
  * Get noise role
  * @return NOISE_ROLE_RESPONDER or NOISE_ROLE_INITIATOR
  */
  virtual int _GetNoiseRole() = 0;

  /**
  * _CheckHandshakeState is separated from GetHandshake
  * because it contains SPLIT state processing
  */
  void _CheckHandshakeState();

  void _ResetHandshake();

  NoiseHandshakeState *m_noise_handshake_state = nullptr;

  NoiseCipherState *m_encrypt_cipher = nullptr;
  NoiseCipherState *m_decrypt_cipher = nullptr;
};

}
