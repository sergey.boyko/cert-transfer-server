/**
* Created by Sergey O. Boyko on 10.02.19.
*/

#pragma once

#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/algorithm/string.hpp>

namespace crypto {

inline std::vector<char> Decode64(std::string_view val) {
  using namespace boost::archive::iterators;
  using It = transform_width<binary_from_base64<std::string::const_iterator>, 8, 6>;

  const std::string base64_padding[] = {"", "==", "="};
  return boost::algorithm::trim_right_copy_if(
      std::vector<char>(It(std::begin(val)), It(std::end(val))),
      [](char c) {
        return c == '\0';
      });
}

inline std::string Encode64(const std::vector<char> &data) {
  using namespace boost::archive::iterators;
  typedef std::vector<char>::const_iterator iterator_type;
  typedef base64_from_binary<transform_width<iterator_type, 6, 8> > base64_enc;

  const std::string base64_padding[] = {"", "==", "="};
  std::stringstream ss;
  std::copy(base64_enc(data.begin()),
            base64_enc(data.end()),
            std::ostream_iterator<char>(ss));

  ss << base64_padding[data.size() % 3];
  return ss.str();
}

}
